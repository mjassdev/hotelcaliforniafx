package application;

import java.io.IOException;
import java.time.LocalDate;

import javax.persistence.EntityManager;

import factory.JPAFactory;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.ScrollPane;
import javafx.scene.image.Image;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.stage.Screen;
import javafx.stage.Stage;
import model.Perfil;
import model.Usuario;
import application.Util;

public class Main extends Application {

	private static BorderPane root;
	
	public static void main(String[] args) {
		launch(args);
	}
	
	@Override
	public void start(Stage primaryStage) throws Exception {
		try {
	        Usuario user = new Usuario();
	        user.setId(1);
	        user.setCpf("123");
			user.setSenha(Util.encrypt("123"));
			user.setNome("Janio Junior");
			user.setPerfil(Perfil.ADMINISTRADOR);
			user.setDataAniversario(LocalDate.now());
			EntityManager em = JPAFactory.getEntityManager();
			em.getTransaction().begin();
			em.persist(user);
			em.getTransaction().commit();
			em.close();
		}catch(Exception e){
			System.out.println("Usuario adicionado: 123\nSenha: 123");
		}
		
		root = FXMLLoader.load(getClass().getResource("/view/main2.fxml"));
		
		Parent clienteView = FXMLLoader.load(getClass().getResource("/view/quartos2.fxml"));
		
		// adicionando a tela inicial no template (parte central)
		ScrollPane scroll = (ScrollPane) root.getChildren().get(1);
		scroll.setFitToHeight(true);
		scroll.setFitToWidth(true);
		VBox vbox = new VBox();
		vbox.setAlignment(Pos.TOP_CENTER);
		vbox.getChildren().add(clienteView);
		scroll.setContent(vbox);
		
		
		Scene scene = new Scene(root, 600, 600);
		
		primaryStage.setTitle("HOTEL CALIFORNIA");
		primaryStage.setScene(scene);
		
		//Full Screen
//		primaryStage.setFullScreen(true);
		primaryStage.setHeight(Screen.getPrimary().getVisualBounds().getHeight());
		primaryStage.setWidth(Screen.getPrimary().getVisualBounds().getWidth());

		primaryStage.show();
	}

//	@Override
//	public void start(Stage primaryStage) throws Exception {
//		
//		AnchorPane root = FXMLLoader.load(getClass().getResource("/view/CadastroCliente.fxml"));
//		
//		Scene scene = new Scene(root, 600, 600);
//		
//		primaryStage.setTitle("CRUD de Clientes");
//		primaryStage.setScene(scene);
//		primaryStage.show();
//	}

}
