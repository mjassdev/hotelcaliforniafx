package application;


import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import com.jfoenix.controls.JFXButton;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.input.MouseEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public class Util {
	
    @FXML private Label tfMensagem;
    @FXML private JFXButton btFechar;
	
	public static String encrypt(String value) {
		try {
			// Classe utilizada para gerar a criptografia em hash
			MessageDigest messageDigest = MessageDigest.getInstance("SHA-256");
			byte[] digest = messageDigest.digest(value.getBytes("UTF-8"));
			
			// convertendo um array de bytes em hexadecimal
			StringBuilder stringBuilder = new StringBuilder();
			for (byte b : digest) {
				stringBuilder.append(String.format("%02X", 0xFF & b));
			}
			
			String senhaHexadecimal = stringBuilder.toString();
			return senhaHexadecimal;
			
		} catch (NoSuchAlgorithmException | UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return "Erro ao encriptar a senha";
	}
	
	public static Alert errorAlert(String mesage) {
		Alert alert = new Alert(AlertType.ERROR);
		alert.setTitle("Aviso");
		alert.setHeaderText(null);
		alert.setContentText(mesage);
		return alert;
		
	}
	
	public void alertaErro(String msg) throws IOException {
		FXMLLoader fXMLLoader = new FXMLLoader();
		fXMLLoader.setLocation(getClass().getResource("/view/valid.fxml"));
		tfMensagem.setText(msg);
		Stage stage = new Stage();
		Scene scene = new Scene(fXMLLoader.load());
		stage.setScene(scene);
		stage.setResizable(false);
		stage.initStyle(StageStyle.UNDECORATED);
		stage.initModality(Modality.APPLICATION_MODAL);
		stage.show();
	}
	
    @FXML
    void handleFecharJanela(MouseEvent event) {
		Stage stage = (Stage) btFechar.getScene().getWindow(); // Obtendo a janela atual
		stage.close(); // Fechando o Stage
    }

	
}