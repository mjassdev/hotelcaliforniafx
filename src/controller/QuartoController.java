package controller;

import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

import org.controlsfx.glyphfont.FontAwesome;

import com.jfoenix.controls.JFXButton;

import de.jensd.fx.glyphs.GlyphsDude;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIcon;
import factory.JPAFactory;
import factory.QuartoControllerFactory;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.LabelBuilder;
import javafx.scene.effect.DropShadow;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import model.Cliente;
import model.Quarto;
import model.Reserva;
import model.TipoQuarto;
import repository.QuartoRepository;
import repository.ReservaRepository;
import styles.AwesomeDude;
import styles.AwesomeIcons;

public class QuartoController extends ControllerSuper implements Initializable {
	@FXML private FlowPane quartosPane;
	private Quarto quarto;
	private Parent parent;
	private JFXButton botaoQuarto;
	private FontAwesome iconeUsuario = new FontAwesome("\uf001");
	HBox hbox = new HBox();
    @FXML private VBox vbHospedes;
    @FXML private FontAwesomeIcon iconHospede;
    @FXML private Label lbNumQuarto;
    @FXML private Label lbTipoQuarto;

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		preencherBotoes();
		listarOcupados();
	}

	public void removerBotoes() {
		GraphicsDevice gd = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice();
		int width = gd.getDisplayMode().getWidth();
		int height = gd.getDisplayMode().getHeight();
		QuartoRepository repository = new QuartoRepository(JPAFactory.getEntityManager());
		List<Quarto> listaQuarto = repository.getQuartos();
		for (Quarto lista : listaQuarto) {
			
			botaoQuarto.setText(lista.getNumeroQuarto());

			quartosPane.getChildren().remove(botaoQuarto);
		}
	}

    
	public void listarOcupados() {
		ReservaRepository repository = new ReservaRepository(JPAFactory.getEntityManager());
		List<Reserva> listaReserva = repository.getQuartoOcupado();
        DropShadow ds1 = new DropShadow();
        ds1.setOffsetY(4.0f);
        ds1.setOffsetX(4.0f);
        ds1.setColor(Color.GRAY);
        vbHospedes.setSpacing(10);

        
        Label pictureLabel = AwesomeDude
                .createIconLabel(AwesomeIcons.ICON_SEARCH, 32);
        
		for(Reserva reserva : listaReserva) {

			ImageView imageView = new ImageView(new Image("/imagens/user.png"));
			hbox = new HBox();
			hbox.setAlignment(Pos.CENTER);
			hbox.setPadding(new Insets(0, 0, 0, 10)); //(top/right/bottom/left)
			hbox.setPrefHeight(50);
			hbox.setEffect(ds1);
			hbox.setStyle("-fx-background-radius: 25px; -fx-background-color: #008B8B");
			imageView.setFitHeight(20);
			imageView.setFitWidth(20);
			Label cliente = new Label(reserva.getCliente().getNome().split(" ")[0]);
			cliente.setStyle("-fx-text-fill: white");
			cliente.setPadding(new Insets(0, 20, 0, 20)); //(top/right/bottom/left)
			Label numeroQuarto = new Label(reserva.getQuarto().getNumeroQuarto());
			numeroQuarto.setStyle("-fx-text-fill: white");
			hbox.getChildren().addAll(imageView, cliente, numeroQuarto);
			vbHospedes.getChildren().add(hbox);
		}
		
	}

	public void preencherBotoes() {
		
		GraphicsDevice gd = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice();
		int width = gd.getDisplayMode().getWidth();
		int height = gd.getDisplayMode().getHeight();
		QuartoRepository repository = new QuartoRepository(JPAFactory.getEntityManager());
		List<Quarto> listaQuarto = repository.getQuartos();
		for (Quarto lista : listaQuarto) {

			botaoQuarto = new JFXButton();
			botaoQuarto.setPrefWidth(width / 15);
			botaoQuarto.setPrefHeight(height / 9);
			atualizaBotoes(botaoQuarto, lista);
			
			botaoQuarto.setText(lista.getNumeroQuarto());
			botaoQuarto.setOnAction(new EventHandler<ActionEvent>() {

				@Override
				public void handle(ActionEvent event) {
					try {
						abrirJanelaQuarto(lista);

					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			});
			quartosPane.getChildren().add(botaoQuarto);

		}
	}

	public static Button atualizaBotoes(Button botaoQuarto, Quarto quartoLista) {
		double imageWidth = 18.0;
		
		if(quartoLista.getNumeroQuarto().equals("69")) {
			System.out.println("");
			
		}
		ReservaRepository repository = new ReservaRepository(JPAFactory.getEntityManager());
		List<Reserva> listaReserva = repository.getQuartoReserva(quartoLista);

		if (listaReserva.isEmpty()) {
			botaoQuarto.setGraphic(null);
			botaoQuarto.setStyle("-fx-background-color:  #008B8B; -fx-background-radius: 0px; -fx-border-color: white");
		} else {
			ImageView imageView = new ImageView(new Image("/imagens/user.png"));
			imageView.setFitHeight(imageWidth);
			imageView.setFitWidth(imageWidth);
			botaoQuarto.setGraphic(imageView);
			botaoQuarto.setStyle("-fx-background-color: #6E6E6E; -fx-text-fill: black;"
					+ "-fx-background-radius: 0px; -fx-border-color: white");
		}
		
		return botaoQuarto;
	}

	private void abrirJanelaQuarto(Quarto quarto) throws IOException {
		CheckController abrirQuarto = QuartoControllerFactory.getInstance();
		abrirQuarto.abrir(quarto); 
		vbHospedes.getChildren().clear();
		quartosPane.getChildren().clear();
		preencherBotoes();
		listarOcupados();
		
	}

	public Parent getParent() {
		return parent;
	}

	public void setParent(Parent parent) {
		this.parent = parent;
	}

	public Quarto getQuarto() {
		return quarto;
	}

	public void setQuarto(Quarto quarto) {
		this.quarto = quarto;
	}
}
