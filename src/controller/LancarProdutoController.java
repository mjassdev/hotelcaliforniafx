package controller;

import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.util.List;
import java.util.ResourceBundle;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXDatePicker;
import com.jfoenix.controls.JFXTextField;

import de.jensd.fx.glyphs.fontawesome.FontAwesomeIcon;
import factory.JPAFactory;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.input.MouseEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import model.Estoque;
import model.Fornecedor;
import model.Lancamento;
import model.Produto;
import repository.FornecedorRepository;
import repository.ProdutoRepository;

public class LancarProdutoController extends ControllerSuper implements Initializable{
	
	private Lancamento lancamento;
	private Produto produto;
	private Estoque estoque;
	
    @FXML private FontAwesomeIcon btFechar;
    @FXML private JFXComboBox<Produto> cbItem;
    @FXML private JFXTextField tfQtd;
    @FXML private JFXTextField tfValorUnit;
    @FXML private JFXDatePicker dpValidade;
    @FXML private JFXComboBox<Fornecedor> cbFornecedor;
    @FXML private JFXButton btIncluir;

	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		carregarComboBox();
		
	}
	
	@FXML
	void handleIncluir(ActionEvent event) throws IOException {

		String nomeProduto = cbItem.getValue().toString();
		ProdutoRepository repository = new ProdutoRepository(JPAFactory.getEntityManager());
		Produto produtoRetorno = repository.getProduto(nomeProduto);
		
		String nomeFornecedor = cbFornecedor.getValue().toString();
		FornecedorRepository repositoryFornecedor = new FornecedorRepository(JPAFactory.getEntityManager());
		Fornecedor fornecedorRetorno = repositoryFornecedor.getFornecedor(nomeFornecedor);
		
		Double precoAtual = Double.parseDouble(tfValorUnit.getText());
		int qtdLancada = Integer.parseInt(tfQtd.getText());
		
		lancamento = new Lancamento(qtdLancada, precoAtual, LocalDate.now(), dpValidade.getValue(), produtoRetorno, fornecedorRetorno);
		
		String itemEstoque = cbItem.getValue().toString();
		ProdutoRepository buscarItemEstoque = new ProdutoRepository(JPAFactory.getEntityManager());
		Estoque estoqueRetorno = buscarItemEstoque.getProdutoEstoque(nomeProduto);
		
		int qtdAntiga = estoqueRetorno.getQuantidadeEmEstoque();
		int novaQuantidade = qtdAntiga + qtdLancada;
		estoqueRetorno.setQuantidadeEmEstoque(novaQuantidade);
		
		Double precoAntigo = estoqueRetorno.getPrecoUnitario();
		Double mediaNovoPreco = (precoAntigo + precoAtual)/2;
		
		estoqueRetorno.setPrecoUnitario(mediaNovoPreco);
		
		super.save(estoqueRetorno);
		
		super.save(lancamento);

	}
	

	
	public void carregarComboBox() {
		ProdutoRepository repository = new ProdutoRepository(JPAFactory.getEntityManager());
		List<Produto> lista = repository.getListProdutos();
		cbItem.setItems(FXCollections.observableArrayList(lista));
		
		FornecedorRepository repositoryFornecedor = new FornecedorRepository(JPAFactory.getEntityManager());
		List<Fornecedor> listaFornecedor = repositoryFornecedor.getFornecedores();
		cbFornecedor.setItems(FXCollections.observableArrayList(listaFornecedor));
		
	}
	
	@FXML
	void handleFecharJanela(MouseEvent event) {
		Stage stage = (Stage) btFechar.getScene().getWindow(); // Obtendo a janela atual
		stage.close(); // Fechando o Stage
	}
	
	public Produto getProduto() {
		return produto;
	}

	public void setProduto(Produto produto) {
		this.produto = produto;
	}
}
