package controller;

import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;

import javax.persistence.EntityManager;

import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXDatePicker;
import com.jfoenix.controls.JFXTextField;

import de.jensd.fx.glyphs.fontawesome.FontAwesomeIcon;
import factory.JPAFactory;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.input.MouseEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import model.Estoque;
import model.Produto;
import model.Unidade;
import repository.UnidadeRepository;

public class CadastroProdutoController  extends ControllerSuper implements Initializable{

	private Produto produto;
	private Estoque estoque;
	private Stage stage;
	private Parent parent;

    @FXML private JFXTextField tfItem;
    @FXML private JFXTextField tfDescricaoProduto;
    @FXML private JFXTextField tfGeneroProdutos;
    @FXML private JFXComboBox<Unidade> tfUnidade;
    @FXML private FontAwesomeIcon btFechar;
	@FXML private Button btLimpar, btExcluir, btAlterar, btIncluir;
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		carregarComboBox();
	}
	
	@FXML
	void handleExcluir(ActionEvent event) throws IOException {
		String item = tfItem.getText(), descricao = tfDescricaoProduto.getText(), genero = tfGeneroProdutos.getText();
		super.remove(produto);
		handleLimpar(event);
	}

	@FXML
	void handleIncluir(ActionEvent event) throws IOException {
		produto = new Produto(tfItem.getText(), tfDescricaoProduto.getText(), tfGeneroProdutos.getText(), tfUnidade.getValue(), LocalDate.now());
		estoque = new Estoque(0,produto, 0.00);
		//super.save(produto);
		super.save(estoque);
		handleLimpar(event);
	}

	@FXML
	void handleLimpar(ActionEvent event) {
		tfItem.setText("");
		tfDescricaoProduto.setText("");
		tfGeneroProdutos.setText("");
		tfUnidade.setValue(null);
		produto = new Produto();
		
		atualizarBotoes();
	}
	
	@FXML
	void handleAlterar(ActionEvent event) throws IOException {
		String item = tfItem.getText(), descricao = tfDescricaoProduto.getText(), genero = tfGeneroProdutos.getText();
		super.remove(produto);

		produto.setItem(tfItem.getText());
		produto.setDescricao(tfDescricaoProduto.getText());
		produto.setGenero(tfGeneroProdutos.getText());

		super.dialogConfirmaAlteracao();

		EntityManager em = JPAFactory.getEntityManager();
		em.getTransaction().begin();
		em.merge(produto);
		em.getTransaction().commit();
		em.close();


		handleLimpar(event);

	}
	
	private void atualizarBotoes() {
		btIncluir.setDisable(produto.getId() != null);
//		btAlterar.setDisable(produto.getId() == null);
		btExcluir.setDisable(produto.getId() == null);
	}
	
	public void carregarComboBox() {

		UnidadeRepository repository = new UnidadeRepository(JPAFactory.getEntityManager());

		List<Unidade> lista = repository.getListUnidades();

		tfUnidade.setItems(FXCollections.observableList(lista));
	}
	
    public void abrir(Produto produto) {
    	
    	setProduto(produto);
    	
    	stage = new Stage();
		Scene scene = new Scene(parent, 500, 345);
		stage.setScene(scene);
		stage.setResizable(false);
		stage.initStyle(StageStyle.UNDECORATED);
		stage.initModality(Modality.WINDOW_MODAL);
		
    	tfItem.setText(produto.getItem());
    	tfDescricaoProduto.setText(produto.getDescricao());
    	tfGeneroProdutos.setText(produto.getGenero());
    	tfUnidade.setValue(produto.getUnidadeMedida());
    	atualizarBotoes();
    	stage.showAndWait();
    }
	
	@FXML
	void handleFecharJanela(MouseEvent event) {
		Stage stage = (Stage) btFechar.getScene().getWindow(); // Obtendo a janela atual
		stage.close(); // Fechando o Stage
	}
	
	public Produto getProduto() {
		return produto;
	}

	public void setProduto(Produto produto) {
		this.produto = produto;
	}

	public Parent getParent() {
		return parent;
	}

	public void setParent(Parent parent) {
		this.parent = parent;
	}
}