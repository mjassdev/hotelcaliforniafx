package controller;

import java.net.URL;
import java.util.ResourceBundle;

import com.jfoenix.controls.JFXButton;

import javafx.fxml.FXML;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

public class fecharController {

    @FXML
    private JFXButton fechar;
    @FXML private JFXButton btOk;
	
	public void initialize(URL location, ResourceBundle resources) {

	}
	
	@FXML
	void handleFecharJanela(MouseEvent event) {
		Stage stage = (Stage) fechar.getScene().getWindow(); // Obtendo a janela atual
		stage.close(); // Fechando o Stage
	}
	
    @FXML
    void handleFecharAviso(MouseEvent event) {
		Stage stage = (Stage) btOk.getScene().getWindow(); // Obtendo a janela atual
		stage.close(); // Fechando o Stage
    }
	
}
