package controller;

import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.ResourceBundle;

import org.controlsfx.control.textfield.TextFields;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXDatePicker;
import com.jfoenix.controls.JFXTextField;
import com.jfoenix.controls.JFXToggleButton;

import de.jensd.fx.glyphs.fontawesome.FontAwesomeIcon;
import factory.JPAFactory;
import factory.QuartoControllerFactory;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import model.Cliente;
import model.Consumo;
import model.Estoque;
import model.Produto;
import model.Quarto;
import model.Reserva;
import repository.ClienteRepository;
import repository.ConsumoRepository;
import repository.ProdutoRepository;
import repository.QuartoRepository;
import repository.ReservaRepository;
import tools.TextFieldFormatter;

public class CheckController extends ControllerSuper implements Initializable {
	private Quarto quarto;
	private Stage stage;
	private Parent parent;
	private Cliente cliente;
	private Reserva reserva;
	private Produto produto;
	private Consumo consumo;
	ActionEvent event;
	
	@FXML private FontAwesomeIcon btFechar;
	@FXML private Button addCliente;
	@FXML private TextField tfNumeroQuarto;
	@FXML private TextField tfHospede;
	@FXML private JFXDatePicker dpEntradaHospede;
	@FXML private JFXDatePicker dpSaidaHospede;
	@FXML private TextArea tfObservacaoHospede;
	@FXML private JFXButton btIniciarEstadia, btFinalizarEstadia;
	@FXML private ComboBox<Produto> cbProdutos;
	@FXML private JFXComboBox<Integer> cbQtdProduto, cbAcomp;
	@FXML private JFXToggleButton tgAcomp, tgVeiculo;
	@FXML private JFXComboBox<Quarto> cbQuartoDisponivel;
	@FXML private JFXTextField tfTipoQuarto;
	@FXML private JFXTextField tfHospedeOut;
	@FXML private JFXDatePicker dpEntradaHospedeOut;
	@FXML private TableView<Consumo> tvProdutoReserva;
	@FXML private TableColumn<Produto, String> tcCodProduto;
	@FXML private TableColumn<Consumo, String> tcDescProduto;
	@FXML private TableColumn<Produto, Integer> tcUndProduto;
	@FXML private TableColumn<Produto, Double> tcPrecoUnitProduto;
	@FXML private TableColumn<Produto, Double> tcTotalProduto;
	@FXML private JFXButton btLancarProduto;
	@FXML private TableColumn<Consumo, Integer> tcQtdProduto;
	@FXML private TableColumn<Produto, LocalDate> tcDataConsProduto;
	@FXML private Label lbAvisoProduto;
    @FXML private JFXTextField tfPlaca;
    @FXML private JFXTextField totalDiarias;
    @FXML private JFXTextField totalConsumo;
    @FXML private JFXTextField totalGeral;
    @FXML private Label lbDiarias;
    @FXML private Label lbConsumo;
    @FXML private Label lbTotal;

	@FXML
	void addCliente(ActionEvent event) throws IOException {
		FXMLLoader fXMLLoader = new FXMLLoader();
		fXMLLoader.setLocation(getClass().getResource("/view/hospede.fxml"));
		Stage stage = new Stage();
		Scene scene = new Scene(fXMLLoader.load());
		stage.setScene(scene);
		stage.setResizable(false);
		stage.initStyle(StageStyle.UNDECORATED);
		stage.initModality(Modality.APPLICATION_MODAL);
		stage.setTitle("Clientes");
		stage.show();
	}
	
    @FXML
    private void formatarPlaca()  {
    	TextFieldFormatter tff = new TextFieldFormatter();
    	tff.setMask("###.####");
    	tff.setTf(tfPlaca);
    	tff.formatter();
    }

    @FXML
    void habilitaPlaca(MouseEvent event) {
    	if(tgVeiculo.isSelected()) {
    		tfPlaca.setDisable(false);
    	}
    	if(!tgVeiculo.isSelected()) {
    		tfPlaca.setDisable(true);
    	}
    }
    
    @FXML
    void habilitaAcompanhantes(MouseEvent event) {
    	if(tgAcomp.isSelected()) {
    		cbAcomp.setDisable(false);
    	}
    	if(!tgAcomp.isSelected()) {
    		cbAcomp.setDisable(true);
    	}
    }
    
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		carregarComboBox(); // CARREGANDO COMBOBOX
		cbQtdProduto.getItems().addAll(1, 2, 3, 4, 5, 6, 7, 8, 9, 10); // ADICIONANDO VALORES AO COMBOBOX QTD DE
		cbAcomp.getItems().addAll(1, 2, 3, 4, 5); // ADICIONANDO VALORES AO COMBOBOX QTD DE ACOMPANHANTES
		carregarClientes(); // CARREGANDO CLIENTES NO AUTO COMPLETATION
		cbQuartoDisponivel.setVisible(false);
		tcDescProduto.setCellValueFactory(new PropertyValueFactory<>("produto"));
		tcQtdProduto.setCellValueFactory(new PropertyValueFactory<>("quantidade"));
		tcDataConsProduto.setCellValueFactory(new PropertyValueFactory<>("data"));
		lbAvisoProduto.setVisible(false);
		tfPlaca.setDisable(true);
		cbAcomp.setDisable(true);
		tfTipoQuarto.setDisable(true);
		dpSaidaHospede.setDisable(true);
		totalDiarias.setVisible(false);
		totalConsumo.setVisible(false);
		totalGeral.setVisible(false);
		lbDiarias.setVisible(false);
		lbConsumo.setVisible(false);
		lbTotal.setVisible(false);
	}

	@FXML
	void handleIncluir(ActionEvent event) throws IOException {
		String nomeHospede = tfHospede.getText();
		ClienteRepository repository = new ClienteRepository(JPAFactory.getEntityManager());
		Cliente clienteRetorno = repository.getClienteSingle(nomeHospede);

		reserva = new Reserva(clienteRetorno, getQuarto(), LocalDate.now(), dpSaidaHospede.getValue(), cbAcomp.getValue(), tfPlaca.getText(), true);
		super.save(reserva);
				
		Stage stage = (Stage) btFechar.getScene().getWindow(); // OBETENDO JANELA ATUAL
		stage.close(); // FECHANDO STAGE
	}
	
	@FXML
	void handleAlterar(ActionEvent event) throws IOException {

		try {
			getReserva().setDataPrevisaoSaida(dpSaidaHospede.getValue());;
			getReserva().setStatus(false);
			super.save(getReserva());

			dialogConfirmaAlteracao();

			Stage stage = (Stage) btFechar.getScene().getWindow(); // Obtendo a janela atual
			stage.close(); // Fechando o Stage

		} catch (Exception e) {

		}
		
		Stage stage = (Stage) btFechar.getScene().getWindow(); // Obtendo a janela atual
		stage.close(); // Fechando o Stage
	}

	@FXML
	void handleAdicionarProduto(ActionEvent event) {
		if (cbProdutos.getValue() != null && cbQtdProduto.getValue() != null) {
			Consumo consumo = new Consumo();
			lbAvisoProduto.setVisible(false);
			
			String nomeProduto = cbProdutos.getValue().toString();
			ProdutoRepository repository = new ProdutoRepository(JPAFactory.getEntityManager());
			List<Estoque> lista = repository.getListEstoqueFilter(nomeProduto);

			for (Estoque listaProduto : lista) {
				consumo.setEstoque(listaProduto);
			}

			consumo.setQuantidade(cbQtdProduto.getValue());
			consumo.setData(LocalDate.now());
			consumo.setReserva(reserva);

			System.out.print("Produto: " + lista);
			System.out.print("Quantidade: " + cbQtdProduto.getValue());

			if (getReserva().getListaConsumo() == null)
				getReserva().setListaConsumo(new ArrayList<Consumo>());

			getReserva().getListaConsumo().add(consumo);

			super.save(consumo);
			carregarConsumo(reserva);

			cbProdutos.setValue(null);
			cbQtdProduto.setValue(null);
		} else {
			System.out.println("Informe o produto e a quantidade");
			lbAvisoProduto.setVisible(true);
		}

	}

	public void carregarClientes() { // METODO PARA BUSCAR TODOS OS CLIENTES
		ClienteRepository repository = new ClienteRepository(JPAFactory.getEntityManager());
		List<Cliente> listaCliente = repository.getClientesGeral();
		
		TextFields.bindAutoCompletion(tfHospede, listaCliente); // USANDO TEXTFIELD COM AUTOCOMPLETE
	}

	@FXML
	void handleFecharJanela(MouseEvent event) {
		Stage stage = (Stage) btFechar.getScene().getWindow(); // OBETENDO JANELA ATUAL
		stage.close(); // FECHANDO STAGE
	}

	public void setarCliente(Cliente cliente) {
		setCliente(cliente);
		System.out.println("Nome: " + cliente.getNome());
		tfHospede.setText(cliente.getNome());

	}

	public void abrir(Quarto quarto) throws IOException {

		setQuarto(quarto);

		stage = new Stage();
		Scene scene = new Scene(parent, 600, 440);
		stage.setScene(scene);
		stage.setResizable(false);
		stage.initStyle(StageStyle.UNDECORATED);
		stage.initModality(Modality.APPLICATION_MODAL);
		tfNumeroQuarto.setText(quarto.getNumeroQuarto());
		tfTipoQuarto.setText(quarto.getTipoQuarto().toString());

		ReservaRepository repository = new ReservaRepository(JPAFactory.getEntityManager());
		List<Reserva> listaReserva = repository.getQuartoReserva(quarto);

		if (!listaReserva.isEmpty()) {
			dpEntradaHospede.setDisable(true);
			cbProdutos.setDisable(false);
			cbQtdProduto.setDisable(false);
			btLancarProduto.setDisable(false);

			for (Reserva lista : listaReserva) {
				setReserva(lista);

				tfHospede.setText(lista.getCliente().getNome());
				tfHospedeOut.setText(lista.getCliente().getNome());
				dpEntradaHospedeOut.setValue(lista.getDataPrevisaoEntrada());
				dpEntradaHospede.setValue(lista.getDataPrevisaoEntrada());
				cbAcomp.setValue(lista.getAcompanhantes());
				dpSaidaHospede.setValue(LocalDate.now());
				totalDiarias.setVisible(true);
				totalConsumo.setVisible(true);
				totalGeral.setVisible(true);
				lbDiarias.setVisible(true);
				lbConsumo.setVisible(true);
				lbTotal.setVisible(true);
				carregarConsumo(lista);
				
				LocalDate dataEntrada = lista.getDataPrevisaoEntrada();
				
				long qtdDias = ChronoUnit.DAYS.between(LocalDate.now(), dataEntrada);
				if(qtdDias < 0) {
					qtdDias = qtdDias * -1;
				}
				if(qtdDias == 0) {
					qtdDias = 1;		
				}
				
				Double precoDiarias = qtdDias * lista.getQuarto().getTipoQuarto().getPreco();
				
				totalDiarias.setText(precoDiarias.toString());
				System.out.println("Preco diarias: " + precoDiarias);
			}

			btIniciarEstadia.setDisable(true);
			btFinalizarEstadia.setDisable(false);
			tfHospede.setDisable(true);
			tfHospedeOut.setDisable(true);
			dpEntradaHospede.setDisable(true);
			dpEntradaHospedeOut.setDisable(true);
			cbAcomp.setDisable(true);
			tgVeiculo.setDisable(true);
			tgAcomp.setDisable(true);
		} else {
			dpEntradaHospede.setDisable(true);
			dpEntradaHospede.setValue(LocalDate.now());
			cbProdutos.setDisable(true);
			cbQtdProduto.setDisable(true);
			btLancarProduto.setDisable(true);
			btIniciarEstadia.setDisable(false);
			btFinalizarEstadia.setDisable(true);
			tgVeiculo.setDisable(false);
			tgAcomp.setDisable(false);
		}
		stage.showAndWait();
	}

	public void carregarComboBox() {
		ProdutoRepository repository = new ProdutoRepository(JPAFactory.getEntityManager());
		List<Produto> lista = repository.getListProdutos();
		cbProdutos.setItems(FXCollections.observableArrayList(lista));
	}

	public void carregarConsumo(Reserva reserva) {
		ConsumoRepository repository = new ConsumoRepository(JPAFactory.getEntityManager());
		List<Consumo> lista = repository.getListProdutosReserva(reserva);
		tvProdutoReserva.setItems(FXCollections.observableList(lista));
		
	}
	
	public Parent getParent() {
		return parent;
	}

	public void setParent(Parent parent) {
		this.parent = parent;
	}

	public Quarto getQuarto() {
		return quarto;
	}

	public void setQuarto(Quarto quarto) {
		this.quarto = quarto;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public Reserva getReserva() {
		return reserva;
	}

	public void setReserva(Reserva reserva) {
		this.reserva = reserva;
	}

	public Produto getProduto() {
		return produto;
	}

	public void setProduto(Produto produto) {
		this.produto = produto;
	}

}