package controller;

import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.io.IOException;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import com.jfoenix.controls.JFXButton;

import de.jensd.fx.glyphs.fontawesome.FontAwesomeIcon;
import factory.JPAFactory;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import model.DefaultEntity;
import model.Usuario;
import repository.Repository;

public class ControllerSuper<T extends DefaultEntity<? super T>> {

	
	private static Usuario usuario = null;

	public T save(T entity) {
		Repository<T> repository = 
				new Repository<T>(JPAFactory.getEntityManager());
		
		// iniciando a transacao
		repository.getEntityManager().getTransaction().begin();
		entity = repository.save(entity);
		repository.getEntityManager().getTransaction().commit();
		repository.getEntityManager().close();
		
		return entity;
	}
	
	
	public void update(T entity) {
	    EntityManagerFactory f = Persistence.createEntityManagerFactory("HotelCalifornia");
	    EntityManager em = f.createEntityManager();
	    em.getTransaction().begin();
	    em.merge(entity);
	    em.getTransaction().commit();
	    f.close();
	}
	
	public void remove(T entity) {
		Repository<T> repository = 
				new Repository<T>(JPAFactory.getEntityManager());
		
		repository.getEntityManager().getTransaction().begin();
		repository.remove(entity);
		repository.getEntityManager().getTransaction().commit();
		repository.getEntityManager().close();
	}
	
	
	protected void dialogConfirma() throws IOException{
		FXMLLoader fXMLLoader = new FXMLLoader();
		fXMLLoader.setLocation(getClass().getResource("/view/confirmacao.fxml"));
		Stage stage = new Stage();
		Scene scene = new Scene(fXMLLoader.load());
		stage.setScene(scene);
		stage.setResizable(false);
		stage.initStyle(StageStyle.UNDECORATED);
		stage.initModality(Modality.APPLICATION_MODAL);
		stage.show();
	}
	protected void dialogConfirmaAlteracao() throws IOException{
		FXMLLoader fXMLLoader = new FXMLLoader();
		fXMLLoader.setLocation(getClass().getResource("/view/confirmacaoalteracao.fxml"));
		Stage stage = new Stage();
		Scene scene = new Scene(fXMLLoader.load());
		stage.setScene(scene);
		stage.setResizable(false);
		stage.initStyle(StageStyle.UNDECORATED);
		stage.initModality(Modality.APPLICATION_MODAL);
		stage.show();
	}
	protected void dialogErroSenhas() throws IOException{
		FXMLLoader fXMLLoader = new FXMLLoader();
		fXMLLoader.setLocation(getClass().getResource("/view/senhas.fxml"));
		Stage stage = new Stage();
		Scene scene = new Scene(fXMLLoader.load());
		stage.setScene(scene);
		stage.setResizable(false);
		stage.initStyle(StageStyle.UNDECORATED);
		stage.initModality(Modality.APPLICATION_MODAL);
		stage.show();
	}
	
	protected void dialogErro() throws IOException{
		FXMLLoader fXMLLoader = new FXMLLoader();
		fXMLLoader.setLocation(getClass().getResource("/view/erro.fxml"));
		Stage stage = new Stage();
		Scene scene = new Scene(fXMLLoader.load());
		stage.setScene(scene);
		stage.setResizable(false);
		stage.initStyle(StageStyle.UNDECORATED);
		stage.initModality(Modality.APPLICATION_MODAL);
		stage.show();
	}
	
	protected void ajustarPane(AnchorPane pane) {
		GraphicsDevice gd = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice();
		int width = gd.getDisplayMode().getWidth();
		int height = gd.getDisplayMode().getHeight();
		pane.setPrefHeight(height);
		pane.setPrefWidth(width);
	}


	public static Usuario getUsuarioLogado() {
		return usuario;
	}


	public static void setUsuarioLogado(Usuario usuario) {
		ControllerSuper.usuario = usuario;
	}

}
	

