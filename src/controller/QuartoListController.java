package controller;

import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXTextField;

import de.jensd.fx.glyphs.fontawesome.FontAwesomeIcon;
import factory.ClienteControllerFactory;
import factory.JPAFactory;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.ListCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Screen;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import model.Cliente;
import model.Perfil;
import model.Quarto;
import model.TipoQuarto;
import repository.ClienteRepository;
import repository.QuartoRepository;

public class QuartoListController extends ControllerSuper implements Initializable{
	
	private Quarto quarto;
	
    @FXML private AnchorPane paneClientes;
    @FXML private FontAwesomeIcon btFechar;
    @FXML private JFXTextField tfNumQuarto;
    @FXML private JFXComboBox<TipoQuarto> cbTipoQuarto;
    @FXML private JFXTextField tfDescricaoQuarto;
    @FXML private JFXButton btIncluir;
    @FXML private JFXButton btAlterar;
    @FXML private JFXButton btExcluir;
    @FXML private JFXButton btLimpar;
    @FXML private TableView<Quarto> tvQuartos;
    @FXML private TableColumn<Quarto, String> tcNumero;
    @FXML private TableColumn<Quarto, String> tcDescricao;
    @FXML private TableColumn<Quarto, TipoQuarto> tcTipo;

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		btIncluir.setDisable(false);
		btAlterar.setDisable(false);
		btExcluir.setDisable(false);
		btLimpar.setDisable(false);	
		
		tcNumero.setCellValueFactory(new PropertyValueFactory<>("numeroQuarto"));
		tcDescricao.setCellValueFactory(new PropertyValueFactory<>("descricaoQuarto"));
		tcTipo.setCellValueFactory(new PropertyValueFactory<>("tipoQuarto"));
		try {
			atualizar();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		cbTipoQuarto.getItems().addAll(TipoQuarto.values());
		// sobreescrevendo o método que mostra o conteudo do combobox
		cbTipoQuarto.setCellFactory(c -> new ListCell<TipoQuarto>() {
			@Override
			protected void updateItem(TipoQuarto item, boolean empty) {
				super.updateItem(item, empty);
				
				if (item == null || empty)
					setText(null);
				else
					setText(item.getLabel());
			}
		});
		// seobrescreendo o método que mostra o conteudo selecionado
		cbTipoQuarto.setButtonCell(new ListCell<TipoQuarto>() {
			@Override
			protected void updateItem(TipoQuarto item, boolean empty) {
				super.updateItem(item, empty);
				
				if (item == null || empty)
					setText(null);
				else
					setText(item.getLabel());
			}
		});
	}
    
	
	@FXML
	void handleMouseClicked(MouseEvent event) throws IOException {

    	if (event.getButton().equals(MouseButton.PRIMARY)) {
			if (event.getClickCount() == 2) {
	    		DialogClienteController listagem = ClienteControllerFactory.getInstance();
				quarto = tvQuartos.getSelectionModel().getSelectedItem();
				setQuarto(quarto);
				tfNumQuarto.setText(quarto.getNumeroQuarto());
				tfDescricaoQuarto.setText(quarto.getDescricaoQuarto());
				cbTipoQuarto.setValue(quarto.getTipoQuarto());


//	    		listagem.abrir(quarto);
			}
    	}
	}
	
	public void atualizar() throws IOException {
		QuartoRepository repository = new QuartoRepository(JPAFactory.getEntityManager());
		List<Quarto> lista = repository.getQuartos();

		tvQuartos.setItems(FXCollections.observableList(lista));		
	}
	
    @FXML
    void handleAlterar(ActionEvent event) {
    	try {
			getQuarto().getId();
			getQuarto().setNumeroQuarto(tfNumQuarto.getText());
			getQuarto().setTipoQuarto(cbTipoQuarto.getValue());
			getQuarto().setDescricaoQuarto(tfDescricaoQuarto.getText());
		
		super.save(getQuarto());
		atualizar();
		super.dialogConfirmaAlteracao();
		
		Stage stage = (Stage) btFechar.getScene().getWindow(); // Obtendo a janela atual
		stage.close(); // Fechando o Stage

	}
	catch(Exception e) {
		
	}
		Stage stage = (Stage) btFechar.getScene().getWindow(); // Obtendo a janela atual
		stage.close(); // Fechando o Stage
    }

    @FXML
    void handleExcluir(ActionEvent event) throws IOException {
    	super.remove(quarto);
    	handleLimpar(event);
    	atualizar();
    }

    @FXML
    void handleFecharJanela(MouseEvent event) {
		Stage stage = (Stage) btFechar.getScene().getWindow(); // Obtendo a janela atual
		stage.close(); // Fechando o Stage
    }
    


    @FXML
    void handleIncluir(ActionEvent event) throws IOException {
		quarto = new Quarto(tfNumQuarto.getText(), tfDescricaoQuarto.getText(), cbTipoQuarto.getValue());
		String numero= tfNumQuarto.getText();

		
		QuartoRepository repository = new QuartoRepository(JPAFactory.getEntityManager());
		List<Quarto> lista = repository.getQuartosExist(numero);
		
		if (lista.isEmpty()) {
			super.save(quarto);
			atualizar();
			handleLimpar(event);
		}else {
			FXMLLoader fXMLLoader = new FXMLLoader();
			fXMLLoader.setLocation(getClass().getResource("/view/quartoExiste.fxml"));
			Stage stage = new Stage();
			Scene scene = new Scene(fXMLLoader.load());
			stage.setScene(scene);
			stage.setResizable(false);
			stage.initStyle(StageStyle.UNDECORATED);
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.showAndWait();
			
		}

    }
    
	

    @FXML
    void handleLimpar(ActionEvent event) {
    	tfNumQuarto.setText("");
    	tfDescricaoQuarto.setText("");
    	cbTipoQuarto.setValue(null);
		quarto = new Quarto();
    }

	public Quarto getQuarto() {
		return quarto;
	}

	public void setQuarto(Quarto quarto) {
		this.quarto = quarto;
	}


}
