package controller;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXDatePicker;
import com.jfoenix.controls.JFXPasswordField;

import application.Util;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIcon;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ListCell;
import javafx.scene.control.TabPane;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.scene.Parent;
import javafx.scene.Scene;
import model.Perfil;
import model.Usuario;

public class CadastraUsuarioController extends ControllerSuper implements Initializable {
	private Usuario usuario;
	private Stage stage;
	private Parent parent;
	TemplateController atualizar = new TemplateController();
	
    @FXML private JFXDatePicker datePickerAniversario;
    @FXML private FontAwesomeIcon btFechar;
	@FXML private TabPane tablePaneAbas;
	@FXML private TextField tfNome, tfCpf, tfEndereco, tfEmail;
	@FXML private Button btLimpar, btExcluir, btAlterar, btIncluir;
    @FXML private JFXPasswordField pfSenha;
    @FXML private JFXPasswordField pfSenha1;
    
    @FXML private JFXComboBox<Perfil> cbPerfilUsuario;

	@Override
	public void initialize(URL location, ResourceBundle resources) {

		cbPerfilUsuario.getItems().addAll(Perfil.values());
		// sobreescrevendo o método que mostra o conteudo do combobox
		cbPerfilUsuario.setCellFactory(c -> new ListCell<Perfil>() {
			@Override
			protected void updateItem(Perfil item, boolean empty) {
				super.updateItem(item, empty);
				
				if (item == null || empty)
					setText(null);
				else
					setText(item.getLabel());
			}
		});
		// seobrescreendo o método que mostra o conteudo selecionado
		cbPerfilUsuario.setButtonCell(new ListCell<Perfil>() {
			@Override
			protected void updateItem(Perfil item, boolean empty) {
				super.updateItem(item, empty);
				
				if (item == null || empty)
					setText(null);
				else
					setText(item.getLabel());
			}
		});
		
	}

	@FXML
	void handleAlterar(ActionEvent event) throws IOException {

		usuario.setCpf(tfCpf.getText());
		usuario.setNome(tfNome.getText());
		usuario.setEmail(tfEmail.getText());
		usuario.setEndereco(tfEndereco.getText());
		usuario.setSenha(pfSenha.getText());

		super.save(usuario);

//		atualizar.trocaTela(5);
		handleLimpar(event);

	}

	@FXML
	void handleExcluir(ActionEvent event) {
		super.remove(usuario);
		handleLimpar(event);
	}

	@FXML
	void handleIncluir(ActionEvent event) throws IOException {
		
		usuario = new Usuario(tfNome.getText(), tfCpf.getText(), tfEndereco.getText(), tfEmail.getText(),
				Util.encrypt(pfSenha.getText()), cbPerfilUsuario.getValue(), datePickerAniversario.getValue());
		if(pfSenha.getText().equals(pfSenha1.getText())) {
			super.save(usuario);
			super.dialogConfirma();
			handleLimpar(event);
		}else if(!pfSenha.getText().equals(pfSenha1.getText())){
			super.dialogErroSenhas();
		}
		else {
			super.dialogErro();
		}
//		atualizar.trocaTela(5);
		handleLimpar(event);
	}

	@FXML
	void handleFecharJanela(MouseEvent event) {
		Stage stage = (Stage) btFechar.getScene().getWindow(); // Obtendo a janela atual
		stage.close(); // Fechando o Stage
	}

	@FXML
	void handleLimpar(ActionEvent event) {
		tfCpf.setText("");
		tfNome.setText("");
		tfEmail.setText("");
		tfEndereco.setText("");
		datePickerAniversario.setValue(null);
		pfSenha.setText("");
		pfSenha1.setText("");
		usuario = new Usuario();
		atualizarBotoes();
	}
	
    public void abrir(Usuario usuario) {
    	
    	setUsuario(usuario);
    	
    	stage = new Stage();
		Scene scene = new Scene(parent, 500, 345);
		stage.setScene(scene);
		stage.setResizable(false);
		stage.initStyle(StageStyle.UNDECORATED);
		stage.initModality(Modality.WINDOW_MODAL);
		
    	tfNome.setText(usuario.getNome());
    	tfCpf.setText(usuario.getCpf());
    	tfEndereco.setText(usuario.getEndereco());
    	tfEmail.setText(usuario.getEmail());
    	pfSenha.setText(usuario.getSenha());
    	pfSenha1.setText(usuario.getSenha());
//    	datePickerAniversario.setValue(usuario.getDataAniversario());
    	atualizarBotoes();
    	stage.show();

    }

	private void atualizarBotoes() {
		btIncluir.setDisable(usuario.getId() != null);
		btAlterar.setDisable(usuario.getId() == null);
		btExcluir.setDisable(usuario.getId() == null);
	}
	
	public Usuario getUsuario() {
		if(usuario == null)
			usuario = new Usuario();
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	
	public Stage getStage() {
		return stage;
	}

	public void setStage(Stage stage) {
		this.stage = stage;
	}

	public Parent getParent() {
		return parent;
	}

	public void setParent(Parent parent) {
		this.parent = parent;
	}
}

