package controller;

import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXPasswordField;
import com.jfoenix.controls.JFXTextField;

import de.jensd.fx.glyphs.fontawesome.FontAwesomeIcon;
import factory.JPAFactory;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import model.Usuario;
import repository.UsuarioRepository;
import application.Util;

public class LoginController implements Initializable {

	private static BorderPane root;

	int tentativa = 3;
	
	@FXML private Label lbTentativa;
	@FXML private JFXButton btEntrar;
	@FXML private FontAwesomeIcon btFechar;
	@FXML private JFXTextField tfUsuario;
	@FXML private JFXPasswordField tfSenha;
	@FXML private Label erro;
	@FXML private AnchorPane apLogin;
	@FXML private FontAwesomeIcon excCpf;
	@FXML private FontAwesomeIcon excSenha;

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		erro.setVisible(false);
		excCpf.setVisible(false);
		excSenha.setVisible(false);
		lbTentativa.setVisible(false);
	}

	@SuppressWarnings("deprecation")
	@FXML
	public void acessarSistema(ActionEvent event) throws IOException, InterruptedException {
		lbTentativa.setVisible(false);
		erro.setVisible(false);
		excCpf.setVisible(false);
		excSenha.setVisible(false);

		UsuarioRepository repository = new UsuarioRepository(JPAFactory.getEntityManager());
    	Usuario usuario = repository.getUsuario(tfUsuario.getText(),  Util.encrypt(tfSenha.getText()));

		
		System.out.println("Senha Encriptada: " + Util.encrypt(tfSenha.getText()));

		if (usuario != null) {

			ControllerSuper.setUsuarioLogado(usuario);

			Button button = (Button) event.getSource();
			Stage stage = (Stage) button.getScene().getWindow();
			stage.close();

		} else if (tfUsuario.getText().isEmpty() || tfSenha.getText().isEmpty()) {
			tentativa--;
			lbTentativa.setVisible(true);

			if (tentativa == 1)
				lbTentativa.setText("Você tem mais " + tentativa + " tentativa");
			else if (tentativa > 1)
				lbTentativa.setText("Você tem mais " + tentativa + " tentativas");
			else
				System.exit(-1);

			if (tfUsuario.getText().isEmpty()) {
				erro.setVisible(true);
				erro.setText("Preencha todos os campos!");
				excCpf.setVisible(true);
			}
			if (tfSenha.getText().isEmpty()) {
				erro.setVisible(true);
				erro.setText("Preencha todos os campos!");
				excSenha.setVisible(true);
			}
		} else if (usuario == null && !tfUsuario.getText().isEmpty() || !tfSenha.getText().isEmpty()) {
			tentativa--;
			lbTentativa.setVisible(true);

			if (tentativa == 1)
				lbTentativa.setText("Você tem mais " + tentativa + " tentativa");
			else if (tentativa > 1)
				lbTentativa.setText("Você tem mais " + tentativa + " tentativas");
			else
				System.exit(-1);
			erro.setVisible(true);
			erro.setText("CONFIRA SEUS DADOS");
		} else {
			lbTentativa.setVisible(true);
			// lbErroTentativa.setVisible(true);
			tentativa--;

			if (tentativa == 1)
				lbTentativa.setText("Você tem mais " + tentativa + " tentativa");
			else if (tentativa > 1)
				lbTentativa.setText("Você tem mais " + tentativa + " tentativas");
			else
				System.exit(-1);

			erro.setVisible(true);
			erro.setText("ALGO ESTÁ ERRADO!");
		}

	}

	@FXML
	void handleFecharJanela(MouseEvent event) {
		
		System.exit(-1);
//		Stage stage = (Stage) btFechar.getScene().getWindow(); // Obtendo a janela atual
//		stage.close(); // Fechando o Stage
	}
}
