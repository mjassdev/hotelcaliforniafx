package controller;

import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

import com.jfoenix.controls.JFXTextField;

import de.jensd.fx.glyphs.fontawesome.FontAwesomeIcon;
import factory.ClienteControllerFactory;
import factory.JPAFactory;
import factory.QuartoControllerFactory;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import model.Cliente;
import repository.ClienteRepository;

public class BuscarClienteController extends ControllerSuper implements Initializable{
    
	private Cliente cliente;
	
	@FXML private FontAwesomeIcon btPesquisar, btFechar;
    @FXML private TableView<Cliente> tvCliente;
    @FXML private TableColumn<Cliente, String> tcNome;
    @FXML private TableColumn<Cliente, String> tcCpf;
    @FXML private TableColumn<Cliente, String> tcEndereco;
    @FXML private JFXTextField tfPesquisar;

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		tcNome.setCellValueFactory(new PropertyValueFactory<>("nome"));
		tcCpf.setCellValueFactory(new PropertyValueFactory<>("cpf"));
		tcEndereco.setCellValueFactory(new PropertyValueFactory<>("endereco"));
	}
    
    @FXML
    void handleBuscar(MouseEvent event) {
		ClienteRepository repository = new ClienteRepository(JPAFactory.getEntityManager());
		List<Cliente> lista = repository.getClientes(tfPesquisar.getText());
		
		if (lista.isEmpty()) {
			Alert alerta = new Alert(AlertType.INFORMATION);
			alerta.setTitle("Informação");
			alerta.setHeaderText(null);
			alerta.setContentText("A consulta não retornou dados.");
			alerta.show();
		}
		
		tvCliente.setItems(FXCollections.observableList(lista));
    }

    @FXML
    void handleFechar(MouseEvent event) {
		Stage stage = (Stage) btFechar.getScene().getWindow(); // Obtendo a janela atual
		stage.close(); // Fechando o Stage
    }

	@FXML
	void handleMouseClicked(MouseEvent event) throws IOException {

    	if (event.getButton().equals(MouseButton.PRIMARY)) {
			if (event.getClickCount() == 2) {
	    		CheckController listagem = QuartoControllerFactory.getInstance();
				cliente = tvCliente.getSelectionModel().getSelectedItem();
				listagem.setarCliente(cliente);

//	    		listagem.abrir(cliente);
//	    		atualizar();
			}
    	}
	}
	
//	public void atualizar() throws IOException {
//		ClienteRepository repository = new ClienteRepository(JPAFactory.getEntityManager());
//		List<Cliente> lista = repository.getClientes(tfPesquisar.getText());
//		if (lista.isEmpty()) {
//			super.dialogErro();
//		}
//		tvClientes.setItems(FXCollections.observableList(lista));		
//	}
	
	
}
