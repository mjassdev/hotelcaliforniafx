package controller;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import com.jfoenix.controls.JFXButton;

import application.Main;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import model.Perfil;

public class TemplateController implements Initializable {
	public static Parent quartosView;
	public static Parent clientesView;
	public static Parent reservasView;
	public static Parent produtosView;
	public static Parent usuariosView;
	public static Parent configView;
	
    @FXML private ScrollPane scrollPane;
    @FXML private Label usuarioLogado;
    @FXML private Label titulo;
    @FXML private JFXButton btQuartos;
    @FXML private JFXButton btClientes;
    @FXML private JFXButton btReservas;
    @FXML private JFXButton btProdutos;
    @FXML private JFXButton btUsuarios;
    @FXML private JFXButton btConfig;
    @FXML private JFXButton btLogout;
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		// abrindo a tela de login
		abrirTelaLogin();
		btQuartos.setPrefHeight(100);
		
		titulo.setText("QUARTOS");
		
		try {
			quartosView = FXMLLoader.load(Main.class.getResource("/view/quartos2.fxml"));
			clientesView = FXMLLoader.load(Main.class.getResource("/view/cliente2.fxml"));
			reservasView = FXMLLoader.load(Main.class.getResource("/view/reservas.fxml"));
			produtosView = FXMLLoader.load(Main.class.getResource("/view/produtos.fxml"));
			usuariosView = FXMLLoader.load(Main.class.getResource("/view/usuarios.fxml"));
			configView = FXMLLoader.load(Main.class.getResource("/view/settings.fxml"));

		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private void abrirTelaLogin() {
		try {
	    	Stage stage = new Stage(StageStyle.TRANSPARENT);
	    	Parent parent = FXMLLoader.load(Main.class.getResource("/view/login.fxml"));
	    	Scene scene = new Scene(parent, 600, 318);
	    	stage.setTitle("Login");
	    	stage.setScene(scene);
	    	stage.initModality(Modality.APPLICATION_MODAL);
	    	stage.showAndWait();
		} catch (IOException e1) {
			e1.printStackTrace();	
		}
		// atualizando a interface com o usuario logado
		try {
			usuarioLogado.setText(ControllerSuper.getUsuarioLogado().getNome().split(" ")[0]);

		}catch(Exception e) {
			
		}
		
		// bloqueando botoes de conforme o perfil
		if (ControllerSuper.getUsuarioLogado().getPerfil().equals(Perfil.ADMINISTRADOR)) {
			btQuartos.setDisable(false);
			btClientes.setDisable(false);
			btReservas.setDisable(false);
			btProdutos.setDisable(false);
			btUsuarios.setDisable(false);
			btConfig.setDisable(false);

		} else if (ControllerSuper.getUsuarioLogado().getPerfil().equals(Perfil.ATENDENTE)) {
			btQuartos.setDisable(false);
			btClientes.setDisable(false);
			btReservas.setDisable(false);
			btProdutos.setDisable(false);
			btUsuarios.setDisable(true);
			btConfig.setDisable(true);
		}
		
	}

	
	@FXML
	void handleQuarto(ActionEvent event) throws IOException {
		VBox vbox = new VBox();
		vbox.setAlignment(Pos.TOP_CENTER);
		vbox.getChildren().add(quartosView);
		scrollPane.setContent(vbox);
    	titulo.setText("QUARTOS");
		btQuartos.setPrefHeight(100);
		btClientes.setPrefHeight(72);
		btReservas.setPrefHeight(72);
		btProdutos.setPrefHeight(72);
		btUsuarios.setPrefHeight(72);
		btConfig.setPrefHeight(72);
	}
	
    @FXML
    void handleReservas(ActionEvent event) throws IOException {
    	FlowPane vbox = new FlowPane();
		vbox.setAlignment(Pos.TOP_CENTER);
		vbox.getChildren().add(reservasView);
		scrollPane.setContent(vbox);
    	titulo.setText("RESERVAS");
		btQuartos.setPrefHeight(72);
		btClientes.setPrefHeight(72);
		btReservas.setPrefHeight(100);
		btProdutos.setPrefHeight(72);
		btUsuarios.setPrefHeight(72);
		btConfig.setPrefHeight(72);

    }

	@FXML
	void handleCliente(ActionEvent event) throws IOException {
		VBox vbox = new VBox();
		vbox.setAlignment(Pos.TOP_CENTER);
		vbox.getChildren().add(clientesView);
		scrollPane.setContent(vbox);
    	titulo.setText("CLIENTES");
		btQuartos.setPrefHeight(72);
		btClientes.setPrefHeight(100);
		btReservas.setPrefHeight(72);
		btProdutos.setPrefHeight(72);
		btUsuarios.setPrefHeight(72);
		btConfig.setPrefHeight(72);
	}
	
	@FXML
	void handleProdutos(ActionEvent event) throws IOException {
		VBox vbox = new VBox();
		vbox.setAlignment(Pos.TOP_CENTER);
		vbox.getChildren().add(produtosView);
		scrollPane.setContent(vbox);
    	titulo.setText("PRODUTOS");
		btQuartos.setPrefHeight(72);
		btClientes.setPrefHeight(72);
		btReservas.setPrefHeight(72);
		btProdutos.setPrefHeight(100);
		btUsuarios.setPrefHeight(72);
		btConfig.setPrefHeight(72);
	}
	
    @FXML
    void handleUsuario(ActionEvent event) throws IOException {
    	FlowPane vbox = new FlowPane();
		vbox.setAlignment(Pos.TOP_CENTER);
		vbox.getChildren().add(usuariosView);
		scrollPane.setContent(vbox);
    	titulo.setText("USUÁRIOS");
		btQuartos.setPrefHeight(72);
		btClientes.setPrefHeight(72);
		btReservas.setPrefHeight(72);
		btProdutos.setPrefHeight(72);
		btUsuarios.setPrefHeight(100);
		btConfig.setPrefHeight(72);

    }
	
    @FXML
    void handleConfiguracoes(ActionEvent event) throws IOException {
    	FlowPane vbox = new FlowPane();
		vbox.setAlignment(Pos.TOP_CENTER);
		vbox.getChildren().add(configView);
		scrollPane.setContent(vbox);
    	titulo.setText("CONFIGURAÇÕES");
		btQuartos.setPrefHeight(72);
		btClientes.setPrefHeight(72);
		btReservas.setPrefHeight(72);
		btProdutos.setPrefHeight(72);
		btUsuarios.setPrefHeight(72);
		btConfig.setPrefHeight(100);
    }
    
    @FXML
    void handleBloquear(ActionEvent event) {
    	abrirTelaLogin();
    }

}
