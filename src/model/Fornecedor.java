package model;

import java.io.Serializable;
import java.time.LocalDate;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

@Entity
public class Fornecedor extends DefaultEntity<Fornecedor> implements Serializable{

	private static final long serialVersionUID = -7917042450982860948L;

	private String responsavel;
	private String email;
	private String nomeFantasia;
	private String razaoSocial;
	private String observacao;
	private String cnpj;
	private String enderecoFornecedor;
	
	private LocalDate dataDeCadastro;
	
	public Fornecedor() {
		
	}
	
	public Fornecedor(String responsavel, String email, String nomeFantasia,
			String razaoSocial, String observacao, String cnpj, String enderecoFornecedor, LocalDate dataDeCadastro) {
		
		this.responsavel = responsavel;
		this.email = email;
		this.nomeFantasia = nomeFantasia;
		this.razaoSocial = razaoSocial;
		this.observacao = observacao;
		this.cnpj = cnpj;
		this.enderecoFornecedor = enderecoFornecedor;
		this.dataDeCadastro = dataDeCadastro;
	}




	public String getResponsavel() {
		return responsavel;
	}

	public void setResponsavel(String responsavel) {
		this.responsavel = responsavel;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getNomeFantasia() {
		return nomeFantasia;
	}

	public void setNomeFantasia(String nomeFantasia) {
		this.nomeFantasia = nomeFantasia;
	}

	public String getRazaoSocial() {
		return razaoSocial;
	}

	public void setRazaoSocial(String razaoSocial) {
		this.razaoSocial = razaoSocial;
	}

	public String getCnpj() {
		return cnpj;
	}

	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}





	public LocalDate getDataDeCadastro() {
		return dataDeCadastro;
	}

	public void setDataDeCadastro(LocalDate dataDeCadastro) {
		this.dataDeCadastro = dataDeCadastro;
	}

	public String getObservacao() {
		return observacao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	public String getEnderecoFornecedor() {
		return enderecoFornecedor;
	}

	public void setEndereçoFornecedor(String enderecoFornecedor) {
		this.enderecoFornecedor = enderecoFornecedor;
	}

    public String toString() {
        return this.getRazaoSocial();
    }

}
