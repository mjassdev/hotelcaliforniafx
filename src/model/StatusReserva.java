package model;

public enum StatusReserva {
	OCUPADO(0, "Ocupado"),
	LIVRE(1, "Livre");

	private int id;
	private String label;

	private StatusReserva(int id, String label) {
		this.setId(id);
		this.setLabel(label);
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}
}
