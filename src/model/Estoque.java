package model;

import java.io.Serializable;
import java.time.LocalDate;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.MapsId;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

@Entity
public class Estoque extends DefaultEntity<Estoque> implements Serializable{

	private static final long serialVersionUID = 1596291306294434440L;
	
	@OneToOne(cascade=CascadeType.ALL)
	@JoinColumn(name="idlancamento")
	private Lancamento lancamento;
	
	private Integer quantidadeEmEstoque;
	
	private Produto produto;
	
	private Double precoUnitario;
	

	public Estoque() {
		
	}
	
	public Estoque(Integer quantidadeEmEstoque, Produto produto, Double precoUnitario) {
		super();
		this.setQuantidadeEmEstoque(quantidadeEmEstoque);
		this.setProduto(produto);
		this.setPrecoUnitario(precoUnitario);
	}


	public Integer getLancamento() {
		return lancamento.getQuantidadeLancamento();
	}

	public void setLancamento(Lancamento lancamento) {
		this.lancamento = lancamento;
	}

	public Produto getProduto() {
		return produto;
	}

	public void setProduto(Produto produto) {
		this.produto = produto;
	}

	public Double getPrecoUnitario() {
		return precoUnitario;
	}

	public void setPrecoUnitario(Double precoUnitario) {
		this.precoUnitario = precoUnitario;
	}

	public Integer getQuantidadeEmEstoque() {
		return quantidadeEmEstoque;
	}

	public void setQuantidadeEmEstoque(Integer quantidadeEmEstoque) {
		this.quantidadeEmEstoque = quantidadeEmEstoque;
	}
	
	
	
	
	
	
}
