package model;

import java.io.Serializable;

import javax.persistence.Entity;

@Entity
public class Quarto extends DefaultEntity<Quarto> implements Serializable{

	private static final long serialVersionUID = 5083196462005516393L;

	private String numeroQuarto;
	private String descricaoQuarto;
	private TipoQuarto tipoQuarto;
	
	public Quarto() {
		
	}
	
	public Quarto(String numeroQuarto, String descricaoQuarto, TipoQuarto tipoQuarto) {
		this.setNumeroQuarto(numeroQuarto);
		this.setDescricaoQuarto(descricaoQuarto);
		this.setTipoQuarto(tipoQuarto);
	}
	
	public String getNumeroQuarto() {
		return numeroQuarto;
	}
	public void setNumeroQuarto(String numeroQuarto) {
		this.numeroQuarto = numeroQuarto;
	}
	public String getDescricaoQuarto() {
		return descricaoQuarto;
	}
	public void setDescricaoQuarto(String descricaoQuarto) {
		this.descricaoQuarto = descricaoQuarto;
	}
	public TipoQuarto getTipoQuarto() {
		return tipoQuarto;
	}
	public void setTipoQuarto(TipoQuarto tipoQuarto) {
		this.tipoQuarto = tipoQuarto;
	}
	
}
