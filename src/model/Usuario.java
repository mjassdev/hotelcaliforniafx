package model;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.Month;

import javax.persistence.Column;
import javax.persistence.Entity;

@Entity
public class Usuario extends DefaultEntity<Usuario> implements Serializable{

	private static final long serialVersionUID = 2748837857727564816L;
	
	private String nome;
	private String cpf;
	private String endereco;
	private String email;
	private String senha;
	private Perfil perfil;
	
	@Column(columnDefinition="Date")
	private LocalDate dataAniversario;
	
	public Usuario(){
		
	}
	
	public Usuario(String nome, String cpf, String endereco, String email, String senha, Perfil perfil, LocalDate dataAniversario) {
		super();
		this.nome = nome;
		this.cpf = cpf;
		this.endereco = endereco;
		this.email = email;
		this.dataAniversario = dataAniversario;
		this.senha = senha;
		this.perfil = perfil;
	}
	
	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getCpf() {
		return cpf;
	}
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
	public String getEndereco() {
		return endereco;
	}
	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}



	public LocalDate getDataAniversario() {
		return dataAniversario;
	}

	public void setDataAniversario(LocalDate dataAniversario) {
		this.dataAniversario = dataAniversario;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public Perfil getPerfil() {
		return perfil;
	}

	public void setPerfil(Perfil perfil) {
		this.perfil = perfil;
	}


}
