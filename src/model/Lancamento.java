package model;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;

@Entity
public class Lancamento extends DefaultEntity<Lancamento> implements Serializable{

	private static final long serialVersionUID = -3451157042493169929L;

	@ManyToOne(cascade=CascadeType.ALL)
	@JoinColumn(name="idproduto")
	private Produto produto;
	
	@ManyToOne
	@JoinColumn(name="idfornecedor")	
	private Fornecedor fornecedor;
	
	@Column(columnDefinition="Date")
	private LocalDate dataDeLancamento;
	
	@Column(columnDefinition="Date")
	private LocalDate dataVencimento;
	
	private Integer quantidadeLancamento;
	
	private double precoUnitario;
	
	
	public Lancamento() {
		
	}
	
	public Lancamento(Integer quantidadeLancamento, double precoUnitario, LocalDate dataDeLancamento, LocalDate dataVencimento, Produto produto, Fornecedor fornecedor) {
		super();

		this.quantidadeLancamento = quantidadeLancamento;
		this.precoUnitario = precoUnitario;
		this.dataDeLancamento = dataDeLancamento;
		this.dataVencimento = dataVencimento;
		this.produto = produto;
		this.fornecedor = fornecedor;
		
	}	
	

	public Fornecedor getFornecedor() {
		return fornecedor;
	}

	public void setFornecedor(Fornecedor fornecedor) {
		this.fornecedor = fornecedor;
	}

	public LocalDate getDataDeLancamento() {
		return dataDeLancamento;
	}

	public void setDataDeLancamento(LocalDate dataDeLancamento) {
		this.dataDeLancamento = dataDeLancamento;
	}

	public LocalDate getDataVencimento() {
		return dataVencimento;
	}

	public void setDataVencimento(LocalDate dataVencimento) {
		this.dataVencimento = dataVencimento;
	}


	public Integer getQuantidadeLancamento() {
		return quantidadeLancamento;
	}

	public void setQuantidadeLancamento(Integer quantidadeLancamento) {
		this.quantidadeLancamento = quantidadeLancamento;
	}

	public double getPrecoUnitario() {
		return precoUnitario;
	}

	public void setPrecoUnitario(double precoUnitario) {
		this.precoUnitario = precoUnitario;
	}	
	
	public Produto getProduto() {
		return produto;
	}

	public void setProduto(Produto produto) {
		this.produto = produto;
	}
	
}
