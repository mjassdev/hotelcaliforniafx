package model;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
public class Consumo extends DefaultEntity<Consumo> {

	private static final long serialVersionUID = -2642581266924811232L;

	@Column(columnDefinition="Date")	
	private LocalDate data;
	
	private Produto produto;
	
	private int quantidade;

	@ManyToOne
	@JoinColumn(name="idreserva")
	private Reserva reserva;

	
	public Consumo() {
		
	}

	public Consumo (Produto produto, int quantidade, LocalDate data) {
		this.produto = produto;
		this.quantidade = quantidade;
		this.data = data;
	}
	
	

	public LocalDate getData() {
		return data;
	}



	public void setData(LocalDate data) {
		this.data = data;
	}



	public Produto getProduto() {
		return produto;
	}



	public void setEstoque(Produto produto) {
		this.produto = produto;
	}



	public int getQuantidade() {
		return quantidade;
	}



	public void setQuantidade(int quantidade) {
		this.quantidade = quantidade;
	}

	public Reserva getReserva() {
		return reserva;
	}

	public void setReserva(Reserva reserva) {
		this.reserva = reserva;
	}
	
	
//	@OneToMany(mappedBy="consumo", cascade=CascadeType.ALL, orphanRemoval=true)
//	private List<ItemConsumo> itemConsumo;





}
