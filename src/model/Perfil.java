package model;

public enum Perfil {

	ADMINISTRADOR(0, "Administrador"),
	ATENDENTE(1, "Atendente");

	private int id;
	private String label;

	private Perfil(int id, String label) {
		this.id = id;
		this.label = label;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}
}
