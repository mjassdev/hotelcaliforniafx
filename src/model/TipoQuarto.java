package model;

public enum TipoQuarto {

	SIMPLES(0, "Simples", 70.00),
	DUPLO(1, "Duplo", 90.00),
	TRIPLO(2, "Triplo", 120.00);

	private int id;
	private String label;
	private double preco;



	private TipoQuarto(int id, String label, double preco) {
		this.id = id;
		this.label = label;
		this.preco = preco;

	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public double getPreco() {
		return preco;
	}

	public void setPreco(double preco) {
		this.preco = preco;
	}
	

}