package model;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

@Entity
public class Reserva extends DefaultEntity<Reserva> implements Serializable{
    
	private static final long serialVersionUID = -6210097564831085196L;

	@OneToOne
	@JoinColumn(name="idcliente")
	private Cliente cliente;
	
	@Column(columnDefinition="Date")
	private LocalDate dataPrevisaoEntrada;
	
	@Column(columnDefinition="Date")
	private LocalDate dataPrevisaoSaida;

	private Integer acompanhantes;
	
	@ManyToOne
	@JoinColumn(name="idquarto")
	private Quarto quarto;
	
	private boolean status;
	
	@ManyToMany(cascade=CascadeType.ALL)
	private List<Consumo> listaConsumo;
	
	private String placaCarro;
	
	
	
	public Reserva() {
		
	}
	
	public Reserva(Cliente cliente, Quarto quarto, LocalDate dataPrevisaoEntrada, LocalDate dataPrevisaoSaida, Integer acompanhantes, String placaCarro, boolean status) {
		super();
		this.cliente = cliente;
		this.quarto = quarto;
		this.dataPrevisaoEntrada = dataPrevisaoEntrada;
		this.dataPrevisaoSaida = dataPrevisaoSaida;
		this.acompanhantes = acompanhantes;
		this.placaCarro = placaCarro;
		this.status = status;
//		this.setPlacaCarro(placaCarro);
//		this.setStatus(status);
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public LocalDate getDataPrevisaoEntrada() {
		return dataPrevisaoEntrada;
	}

	public void setDataPrevisaoEntrada(LocalDate dataPrevisaoEntrada) {
		this.dataPrevisaoEntrada = dataPrevisaoEntrada;
	}
	public LocalDate getDataPrevisaoSaida() {
		return dataPrevisaoSaida;
	}

	public void setDataPrevisaoSaida(LocalDate dataPrevisaoSaida) {
		this.dataPrevisaoSaida = dataPrevisaoSaida;
	}


	public Integer getAcompanhantes() {
		return acompanhantes;
	}

	public void setAcompanhantes(Integer acompanhantes) {
		this.acompanhantes = acompanhantes;
	}

	public Quarto getQuarto() {
		return quarto;
	}

	public void setQuarto(Quarto quarto) {
		this.quarto = quarto;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}
	
	public List<Consumo> getListaConsumo() {
		return listaConsumo;
	}

	public void setListaConsumo(List<Consumo> listaConsumo) {
		this.listaConsumo = listaConsumo;
	}

	public String getPlacaCarro() {
		return placaCarro;
	}

	public void setPlacaCarro(String placaCarro) {
		this.placaCarro = placaCarro;
	}


}
