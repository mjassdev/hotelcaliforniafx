package repository;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import model.Cliente;
import model.Quarto;

public class ClienteRepository extends Repository<Cliente> {

	public ClienteRepository(EntityManager entityManager) {
		super(entityManager);
		
	}

	public List<Cliente> getClientes(String nome) {
		Query query = getEntityManager().createQuery("SELECT c FROM Cliente c WHERE lower(c.nome) like lower(:nome) Order by c.nome");
		query.setParameter("nome", "%" + nome + "%");

		List<Cliente> lista = query.getResultList();
		if (lista == null) {
			lista = new ArrayList<Cliente>();
		}
		return lista;
	}
	
	
	
	public List<Cliente> getClientesGeral() {
		Query query = getEntityManager().createQuery("SELECT c FROM Cliente c");

		List<Cliente> lista = query.getResultList();
		if (lista == null) {
			lista = new ArrayList<Cliente>();
		}
		return lista;
	}
	
	public Cliente getClienteSingle(String nome2) {
		Cliente cliente = null;
		
		Query query = getEntityManager().createQuery("SELECT c from Cliente c WHERE c.nome=:nome2");
		query.setParameter("nome2", nome2);
		cliente = (Cliente) query.getSingleResult();

		return cliente;
	}

}
