package repository;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;

import model.Cliente;
import model.Quarto;
import model.Reserva;

public class ReservaRepository extends Repository<Reserva> {

	public ReservaRepository(EntityManager entityManager) {
		super(entityManager);
	}

	public List<Reserva> getReservas() {
		Query query = getEntityManager().createQuery("SELECT c FROM Reserva c");

		List<Reserva> lista = query.getResultList();
		if (lista == null) {
			lista = new ArrayList<Reserva>();
		}
		
		return lista;
	}
	
	public List<Reserva> getClienteReserva(Quarto quarto) {
		Query query = getEntityManager().createQuery("SELECT c FROM Reserva c WHERE c.quarto = :quarto AND c.status = true");
		query.setParameter("quarto", quarto);
		List<Reserva> lista = query.getResultList();
		if (lista == null) {
			lista = new ArrayList<Reserva>();
		}
		
		return lista;
	}

	public List<Reserva> getClienteReservaFilter(Cliente cliente) {
		Query query = getEntityManager().createQuery("SELECT c FROM Reserva c WHERE c.cliente = :cliente");
		query.setParameter("cliente", cliente);
		List<Reserva> lista = query.getResultList();
		if (lista == null) {
			lista = new ArrayList<Reserva>();
		}
		
		return lista;
	}
	
	public List<Reserva> getQuartoReserva(Quarto quarto) {
		Query query = getEntityManager().createQuery("SELECT c FROM Reserva c WHERE c.quarto = :quarto AND c.status = true");
		query.setParameter("quarto", quarto);
		
		List<Reserva> lista = query.getResultList();
		if (lista == null) {
			lista = new ArrayList<Reserva>();
		}
		
		return lista;
	}
	
	public List<Reserva> getQuartoOcupado() {
		Query query = getEntityManager().createQuery("SELECT c FROM Reserva c WHERE c.status = true ORDER BY c.quarto");
//		query.setParameter("quarto", quarto);
		
		List<Reserva> lista = query.getResultList();
		if (lista == null) {
			lista = new ArrayList<Reserva>();
		}
		
		return lista;
	}
	
}
