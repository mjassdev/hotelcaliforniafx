package repository;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import model.Consumo;
import model.Estoque;
import model.Produto;
import model.Reserva;
import model.Usuario;

public class ProdutoRepository extends Repository<Produto> {

	public ProdutoRepository(EntityManager entityManager) {
		super(entityManager);

	}

	public List<Produto> getProdutos() {
		Query query = getEntityManager().createQuery("SELECT c FROM Produto c");
		// query.setParameter("descricao", "%" + descricao + "%");

		List<Produto> lista = query.getResultList();
		if (lista == null) {
			lista = new ArrayList<Produto>();
		}
		return lista;
	}

	public List<Estoque> getEstoque() {
		Query query = getEntityManager().createQuery("SELECT c FROM Estoque c");
		// query.setParameter("descricao", "%" + descricao + "%");

		List<Estoque> lista = query.getResultList();
		if (lista == null) {
			lista = new ArrayList<Estoque>();
		}
		return lista;
	}
	
	
	public Produto getProduto(String produto) {
		Query query = 
				getEntityManager().
					createQuery("SELECT u  FROM Produto u WHERE u.item = :produto");
		query.setParameter("produto", produto);

		
		List<Produto> lista = query.getResultList();
	
		if (lista == null || lista.isEmpty())
			return null;
		
		return lista.get(0);
	}
	
	public Estoque getProdutoEstoque(String produto) {
		Query query = 
				getEntityManager().
					createQuery("SELECT u FROM Estoque u WHERE u.produto.item = :produto");
		query.setParameter("produto", produto);

		
		List<Estoque> lista = query.getResultList();
	
		if (lista == null || lista.isEmpty())
			return null;
		
		return lista.get(0);
	}
	
	
	
	public List<Produto> getListProdutos() {

		Query query = getEntityManager().createQuery("SELECT p FROM Produto p");

		List<Produto> lista = query.getResultList();

		if (lista == null) {
			lista = new ArrayList<Produto>();
		}
		return lista;
	}
	
	
	public List<Estoque> getListEstoque() {

		Query query = getEntityManager().createQuery("SELECT p FROM Estoque p");

		List<Estoque> lista = query.getResultList();

		if (lista == null) {
			lista = new ArrayList<Estoque>();
		}
		return lista;
	}

	public List<Produto> getListProdutosFilter(String nome) {

		Query query = getEntityManager().createQuery("SELECT p FROM Produto p WHERE p.item=:nome");
		query.setParameter("nome", nome);
		List<Produto> lista = query.getResultList();

		if (lista == null) {
			lista = new ArrayList<Produto>();
		}
		return lista;
	}
	public List<Estoque> getListEstoqueFilter(String nome) {

		Query query = getEntityManager().createQuery("SELECT p FROM Estoque p WHERE p.item=:nome");
		query.setParameter("nome", nome);
		List<Estoque> lista = query.getResultList();

		if (lista == null) {
			lista = new ArrayList<Estoque>();
		}
		return lista;
	}
}
