package repository;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import model.Usuario;

public class UsuarioRepository extends Repository<Usuario> {

	public UsuarioRepository(EntityManager entityManager) {
		super(entityManager);
		
	}

	public List<Usuario> getUsuarios(String nome) {
		Query query = getEntityManager().createQuery("SELECT c FROM Usuario c WHERE lower(c.nome) like(:nome)");
		query.setParameter("nome", "%" + nome + "%");

		List<Usuario> lista = query.getResultList();
		if (lista == null) {
			lista = new ArrayList<Usuario>();
		}
		return lista;
	}


	public Usuario getUsuario(String login, String senha) {
		Query query = 
				getEntityManager().
					createQuery("SELECT "
							  + "  u "
							  + "FROM "
							  + "  Usuario u "
							  + "WHERE "
							  + "  u.cpf = :login AND u.senha = :senha");
		query.setParameter("login", login);
		query.setParameter("senha", senha);
		
		List<Usuario> lista = query.getResultList();
	
		if (lista == null || lista.isEmpty())
			return null;
		
		return lista.get(0);
	}
	
	
	public List<Usuario> getLogin(String cpf, String senha) {
		Query query = getEntityManager().createQuery("SELECT c FROM Usuario c WHERE lower(c.cpf) like(:cpf) AND lower(c.senha) like(:senha)");
		query.setParameter("cpf",cpf);
		query.setParameter("senha",senha);

		List<Usuario> lista = query.getResultList();
		if (lista == null) {
			lista = new ArrayList<Usuario>();
		}
		return lista;
	}
}
