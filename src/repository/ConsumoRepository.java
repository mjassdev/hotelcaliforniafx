package repository;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import model.Consumo;
import model.Produto;
import model.Reserva;

public class ConsumoRepository extends Repository<Produto> {

	public ConsumoRepository(EntityManager entityManager) {
		super(entityManager);

	}

//	public List<Consumo> getListProdutosReserva(Reserva reserva) {
//
//		Query query = getEntityManager().createQuery("SELECT p FROM Consumo p WHERE p.reserva=:reserva");
//		query.setParameter("reserva", reserva);
//		List<Consumo> lista = query.getResultList();
//
//		if (lista == null) {
//			lista = new ArrayList<Consumo>();
//		}
//		return lista;
//	}
	
	
	public List<Consumo> getListProdutosReserva(Reserva reserva) {

		Query query = getEntityManager().createQuery("SELECT p FROM Consumo p WHERE p.reserva=:reserva");
		query.setParameter("reserva", reserva);
		List<Consumo> lista = query.getResultList();

		if (lista == null) {
			lista = new ArrayList<Consumo>();
		}
		return lista;
	}
}
