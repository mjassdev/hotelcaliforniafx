package repository;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import model.Consumo;
import model.Fornecedor;
import model.Produto;
import model.Reserva;
import model.Usuario;

public class FornecedorRepository extends Repository<Produto> {

	public FornecedorRepository(EntityManager entityManager) {
		super(entityManager);

	}

	public List<Fornecedor> getFornecedores() {
		Query query = getEntityManager().createQuery("SELECT c FROM Fornecedor c");
		// query.setParameter("descricao", "%" + descricao + "%");

		List<Fornecedor> lista = query.getResultList();
		if (lista == null) {
			lista = new ArrayList<Fornecedor>();
		}
		return lista;
	}

	public Fornecedor getFornecedor(String fornecedor) {
		Query query = 
				getEntityManager().
					createQuery("SELECT "
							  + "  u "
							  + "FROM "
							  + "  Fornecedor u "
							  + "WHERE "
							  + "  u.razaoSocial = :fornecedor");
		query.setParameter("fornecedor", fornecedor);

		
		List<Fornecedor> lista = query.getResultList();
	
		if (lista == null || lista.isEmpty())
			return null;
		
		return lista.get(0);
	}
	


	public List<Fornecedor> getListFornecedorFilter(String fornecedor) {

		Query query = getEntityManager().createQuery("SELECT p FROM Fornecedor p WHERE p.razaoSocial=:fornecedor");
		query.setParameter("fornecedor", fornecedor);
		List<Fornecedor> lista = query.getResultList();

		if (lista == null) {
			lista = new ArrayList<Fornecedor>();
		}
		return lista;
	}

}
