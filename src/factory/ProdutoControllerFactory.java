package factory;

import java.io.IOException;

import controller.CadastroProdutoController;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;

public class ProdutoControllerFactory {
	public static CadastroProdutoController getInstance() throws IOException {
    	
    	FXMLLoader loader = new FXMLLoader(ProdutoControllerFactory.class.getClass().getResource("/view/novoproduto.fxml"));
    
		Parent root = loader.load();

		CadastroProdutoController listagem = loader.getController();	
    	listagem.setParent(root);
		
		return listagem;
	}
}
